import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

Item {
    id: registerPage
    ColumnLayout {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 20
        anchors.top: parent.top

        RowLayout {
            Image {
                id: logoImage
//              Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                source: "images/logo.png"
                anchors.top: parent.top
            }
            Label {
                text: qsTr("Create new KeepSolid ID")
//              verticalAlignment: Text.AlignVCenter
//              horizontalAlignment: Text.AlignHCenter
//              Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
//              elide: Label.ElideRight
                Layout.fillWidth: true
            }
        }
        TextField {
            id: emailField
//            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            placeholderText: qsTr("Email*")
        }
        TextField {
            id: passwordField
//            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            placeholderText: qsTr("Password*")
        }
        TextField {
            id: confirmField
//            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            placeholderText: qsTr("Confirm Password*")
        }
        TextField {
            id: nameField
//            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            placeholderText: qsTr("Name*")
        }
        TextField {
            id: surnameField
//            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            placeholderText: qsTr("Surname*")
        }
        TextField {
            id: companyField
//            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            placeholderText: qsTr("Company name")
        }

        Button {
            id: registerButton
            text: qsTr("Sign Up")
//            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            onClicked: {
                console.log("Sign Up Button Pressed")
            }
        }
        Button {
            id: cancelButton
            text: qsTr("Cancel")
//            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            onClicked: {
                console.log("Cancel Button Pressed")
            }
        }
    }
}
