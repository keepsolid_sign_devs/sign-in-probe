import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

ApplicationWindow {
    id: loginWindow
    visible: true
    width: 370
    height: 560
    title: qsTr("KeepSolid Sign")

    Rectangle {
        id: contentView
        width: parent.width
        height: parent.height - agreementBar.height
        anchors.fill: parent
        focus: true
        property string title: "Sign In"
        property int index: 0

        Loader {
            id: resourcesLoader
            anchors.fill: parent
            source: resourcesList.get(contentView.index).resource
            property bool valid: item !== null
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                contentView.index = (contentView.index == resourcesList.count-1)? 0 : contentView.index+1
                contentView.title = resourcesList.get(contentView.index).title
                resourcesLoader.source = resourcesList.get(contentView.index).resource
            }
        }
    }

    footer: ToolBar {
        id: agreementBar
        height: descLabel.implicitHeight*2
        RowLayout {
            anchors.fill: parent
            height: parent.height
            Label {
                id: descLabel
                text: qsTr("By selecting ") + contentView.title + qsTr(", you agree to the <a href='google.com'>Tearms and Conditions</a>")
                elide: Label.ElideRight
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                Layout.fillWidth: true
            }
        }
    }

    ListModel {
        id: resourcesList
        ListElement { title: "Sign In"; resource: "qrc:/SignInPage.qml" }
        ListElement { title: "Sign Up"; resource: "qrc:/SignUpPage.qml" }
        ListElement { title: "Sign Up"; resource: "qrc:/SignUpdatePage.qml" }
    }
}
