import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

Item {
    id: loginPage
    ColumnLayout {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 20
        anchors.top: parent.top

        Image {
            id: logoImage
//            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            source: "images/logo.png"
            anchors.top: parent.top
        }
        Label {
            text: qsTr("Sign in with KeepSolid ID")
//            verticalAlignment: Text.AlignVCenter
//            horizontalAlignment: Text.AlignHCenter
//            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
//            elide: Label.ElideRight
            Layout.fillWidth: true
        }
        TextField {
            id: loginField
//            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            placeholderText: qsTr("login")
        }
        TextField {
            id: passwordField
//            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            placeholderText: qsTr("password")
        }
        CheckBox {
            text: "Remember password"
//            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        }

        Button {
            id: loginButton
            text: qsTr("Sign In")
//            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            onClicked: {
                console.log("Login Button Pressed")
            }
        }
    }
}
